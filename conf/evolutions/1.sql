
-- !Ups
CREATE TABLE cars (
	id serial NOT NULL PRIMARY KEY,
	reg_no varchar(9) NULL,
	brand varchar(20) NULL,
	color varchar(20) NULL,
	production_year int2 NULL
);

-- !Downs
DROP TABLE cars;