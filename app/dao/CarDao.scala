package dao

import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import models.Car

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class CarDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) (implicit ec: ExecutionContext){
  private val dbConfig = dbConfigProvider.get[JdbcProfile]
  import dbConfig._
  import profile.api._

  private class CarTable(tag: Tag) extends Table[Car](tag, "cars") {
    def id: Rep[Int] = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def reg_no: Rep[String] = column[String]("reg_no")
    def brand: Rep[String] = column[String]("brand")
    def color: Rep[String] = column[String]("color")
    def production_year: Rep[Int] = column[Int]("production_year")

    override def * = (id.?, reg_no, brand, color, production_year).mapTo[Car]
  }

  private val cars = TableQuery[CarTable]

  def getAllCars: Future[Seq[Car]] =  db.run(cars.result)

  def isRegNoUnique(reg_no: String): Future[Boolean] = db.run(cars.filter(_.reg_no === reg_no).exists.result).map(!_)

  def addCar(car: Car):Future[Unit] = db.run(cars += car).map(_ => ())

  def deleteCar(reg_no: String): Future[Int] = db.run(cars.filter(_.reg_no === reg_no).delete)

  def amountOfRecords: Future[Int] = db.run(cars.size.result)
}
