package controllers

import dao.CarDao
import models.Car
import play.api.libs.json.{JsError, JsValue, Json}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class CarController @Inject() (cc: ControllerComponents, carDao: CarDao)
                              (implicit ec: ExecutionContext)
  extends AbstractController(cc) {
  def getCars: Action[AnyContent] = Action.async { implicit request =>
    carDao.getAllCars.map { cars =>
      val carsJson = Json.toJson(cars)
      Ok(Json.toJson(carsJson))
    }
  }

  def createCar: Action[JsValue] = Action.async(parse.json) { implicit request =>
    val carDataResult = request.body.validate[Car]
    carDataResult.fold(
      errors => Future.successful(BadRequest(Json.obj(
        "data" -> Json.arr(),
        "errors" -> JsError.toJson(errors)
      ))),
      carData => {
        carDao.isRegNoUnique(carData.reg_no).flatMap { isUnique =>
          if (isUnique) {
            carDao.addCar(carData).map { _ =>
              Created(Json.obj("data" -> "Car added successfully", "errors" -> Json.arr()))
            }
          }
          else {
            Future.successful(BadRequest(Json.obj(
              "data" -> Json.arr(),
              "errors" -> Json.arr("Car with such reg number already exists")
            )))
          }
        }
      }
    )
  }

  def deleteCar(reg_no: String): Action[AnyContent] = Action.async { implicit request =>
    carDao.deleteCar(reg_no).map { x =>
      Ok(Json.obj("data" -> s"Deleted $x rows", "errors" -> Json.arr()))
    }
  }

  def getStats: Action[AnyContent] = Action.async { implicit request =>
    carDao.amountOfRecords.map { x =>
      Ok(Json.obj("data" -> Json.obj("records_amount" -> x), "errors" -> Json.arr()))
    }
  }
}
