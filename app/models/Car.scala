package models

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Car(id: Option[Int], reg_no: String, brand: String, color: String, production_year: Int)

object Car {
  implicit val carWrites: Writes[Car] = Json.writes[Car]
  implicit val carReads: Reads[Car] = Json.reads[Car]
}